"""
Made by Martin Martinic
The program is free for personal use and for commercial use with proper credit to the author
This code was made in Python 3.7 using Community PyCharm
"""

from pathlib import Path
from tkinter import *
import numpy as np
import os

root = Tk()


class DataManipulationInTimeline:
    """
    in class DataManipulationInTimeline are functions that read, write, rearrange files used throe timeline

    :var
    all_permits         - list of permits that can one have while using application
    installation_map    - map in witch all installation files are
    timeline_data_map   - sub map in witch files used in creation of timeline are
    data_map            - sum map in witch are timeline events
    """
    all_permits = ["discovered_world_lore", "known_world_events", "campaign_progress",  # all
                   "world_events", "lore_of_the_world",                                 # game_master + spectator
                   "editor_mode"]                                                       # game_master

    # creating all paths used throe this class
    installation_map = Path(".")
    timeline_data_map = installation_map / "timeline_data"
    data_map = timeline_data_map / "data"

    def __init__(self):
        """
        constructor function will crate all needed documents and maps/sub maps needed for program to run correctly if
        thy do not exist
        """

        # creating map for timeline permits and documents "timeline_data"
        try:
            os.mkdir(self.timeline_data_map)
        except FileExistsError:
            print("no need to create another one of thees", self.timeline_data_map)

        # creating permit.csv documents
        for permit in self.all_permits:
            permit_file_name = permit + ".csv"
            csv_permit_location_and_name = self.timeline_data_map / permit_file_name
            try:
                csv_permit = open(csv_permit_location_and_name, "x")
                csv_permit.write("date (dd/mm/yyyy); "
                                 "document associated with that date under this permit (one at hte time)\n")
                csv_permit.close()
            except FileExistsError:
                print(permit_file_name, "already exists")

        # crating sub map of timeline_data witch sores timeline events "data"
        try:
            os.mkdir(self.data_map)
        except FileExistsError:
            print("no need to create another one of thees", self.timeline_data_map)

    def editing_of_timeline(self):
        """

        """

    def finding_permits_one_has(self, role_of_the_observer):
        """
        finding_permits_one_has is a function that will connect ues role to its permits. That way game master, players,
        or spectators my get all needed information.

        :var
        self.all_permits    - list of all permits one can have
        roles_and_permits   - dictionary that connects role of one to its permits
        permits_of_role     - list of permits one have

        :key
        game_master         - one who runes the game
        spectator           - one who is just interested in game world or game and is not actively participant in it
        player              - one who plays game and ads spice to it
        """

        roles_and_permits = {"game_master": self.all_permits[:],
                             "spectator": self.all_permits[:5],
                             "player": self.all_permits[:3]}
        permits_of_role = roles_and_permits[role_of_the_observer]
        return permits_of_role

    def reading_of_permit(self, permit):
        """
        reading_of_permit is a function that will read permit.csv document and return matrix of all documents permitted
        with that permit, as well as dates thous documents have.

        if editor_mode permit was requested it will run editing_of_timeline function instead, and return empty list.

        :var
        list_of_data_under_permit       - returning list that will hold dates, and names od documents permitted to one
        permit_file_name                - variable that ads .csv to the name of permit
        csv_permit_location_and_name    - location on disc where permits are saved
        """

        list_of_data_under_permit = []
        if permit in self.all_permits[:-1]:
            permit_file_name = permit + ".csv"
            csv_permit_location_and_name = self.timeline_data_map / permit_file_name
            list_of_data_under_permit = np.loadtxt(csv_permit_location_and_name, delimiter=";", dtype=str, skiprows=1)

        elif permit == self.all_permits[-1]:
            self.editing_of_timeline()

        else:
            print("unspecified or non existing permit")
        return list_of_data_under_permit



